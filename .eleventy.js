const navigationPlugin = require("@11ty/eleventy-navigation")
const syntaxHighlightingPlugin = require("@11ty/eleventy-plugin-syntaxhighlight")
const markdownIt = require('markdown-it');
const markdownItClass = require('@toycode/markdown-it-class');
const markdownItAnchor = require('markdown-it-anchor')


module.exports = function(eleventyConfig) {
  eleventyConfig.addPlugin(navigationPlugin)
  eleventyConfig.addPlugin(syntaxHighlightingPlugin)

  eleventyConfig.addPassthroughCopy("assets")
  eleventyConfig.addPassthroughCopy("fonts")

  eleventyConfig.addShortcode("fas_icon", function(name) { return `<span class="fas fa-${name}"></span>` })

  const mapping = {
    h2: 'content-title',h3: 'content-title',h4: 'content-title',h5: 'content-title',h6: 'content-title',
    table: 'table',
    blockquote: 'alert'
  };
  
  const mdOptions = { linkify: false, html: true };
  const mdAnchorOpts = { 
    permalink: markdownItAnchor.permalink.headerLink(),
    permalinkClass: 'ml-5', permalinkSymbol: '#', level: [1, 2, 3, 4] 
  }
  
  eleventyConfig.setLibrary(
    'md',
    markdownIt(mdOptions)
      .use(markdownItClass, mapping)
      .use(markdownItAnchor, mdAnchorOpts)
  )
  
  return {
    dir: {
      input: "content"
    }
  }
}
