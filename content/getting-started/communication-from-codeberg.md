---
eleventyNavigation:
  key: CommunicationCodeberg
  title: Communication from Codeberg 
  parent: GettingStarted
  order: 65
---

By default, Codeberg will send all your notifications to your registered email address.

To change your preferences, you can click on your profile on the top-right corner, and from there, navigate to Settings->Account->Manage Email Addresses. Note that disabling e-mail notifications will not disable receiving important communication from Codeberg.
